package net.welovespigotplugins.worldmanager;

import net.welovespigotplugins.worldmanager.commands.WorldManagerCommand;
import net.welovespigotplugins.worldmanager.objects.WorldContainer;
import net.welovespigotplugins.worldmanager.utils.ConfigUtils;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * JavaDoc this file!
 * Created: 04.10.2018
 *
 * @author WeLoveSpigotPlugins (welovespigotplugins@gmail.com)
 */
public class WorldManager extends JavaPlugin {

    public static final String PREFIX = "§8[§bWM§8] ";
    private final ConfigUtils configUtils = new ConfigUtils();

    @Override
    public void onEnable() {

        init();

        registerCommands();

    }

    private void registerCommands() {
        getCommand("wm").setExecutor(new WorldManagerCommand());
    }

    @Override
    public void onDisable() {
        configUtils.saveConfig();
    }

    private void init() {

        getLogger().info("Loading worlds...");
        configUtils.loadConfig();
        getLogger().info("Worlds loaded successfully! (" + WorldContainer.getWorldContainerMap().size() + " Worlds)");

    }

}
