package net.welovespigotplugins.worldmanager.utils;

import com.google.common.collect.Lists;
import net.welovespigotplugins.worldmanager.objects.WorldContainer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * JavaDoc this file!
 * Created: 04.10.2018
 *
 * @author WeLoveSpigotPlugins (welovespigotplugins@gmail.com)
 */
public class ConfigUtils {

    final File file = new File("plugins//WorldManager//worlds.yml");
    final YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(file);

    public void loadConfig() {

        for(String string : yamlConfiguration.getStringList("Worlds")) {

            final WorldContainer worldContainer = new WorldContainer(string);

            if(yamlConfiguration.get(string + ".Location") == null) continue;

            Bukkit.createWorld(new WorldCreator(string));

            worldContainer.setSpawnLocation(deserializeLocation(yamlConfiguration.getString(string + ".Location")));
            worldContainer.loadWorld();

        }

        for(World world : Bukkit.getWorlds()) {
            if(!WorldContainer.getWorldContainerMap().containsKey(world.getName())) {
                WorldContainer.getWorldContainerMap().put(world.getName(), new WorldContainer(world.getName()));
            }
        }

    }

    public void saveConfig() {

        final List<String> worlds = Lists.newCopyOnWriteArrayList();

        for(String containerName : WorldContainer.getWorldContainerMap().keySet()) {

            final WorldContainer worldContainer = WorldContainer.getWorldContainerMap().get(containerName);

            worlds.add(containerName);

            if(worldContainer.getSpawnLocation() == null) continue;

            yamlConfiguration.set(containerName + ".Location", serializeLocation(worldContainer.getSpawnLocation()));

        }

        yamlConfiguration.set("Worlds", worlds);

        try {
            yamlConfiguration.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private String serializeLocation(final Location location) {
        return location.getWorld().getName() + ":" + location.getX() + ":" + location.getY() + ":" + location.getZ() + ":" + location.getYaw() + ":" + location.getPitch();
    }

    public Location deserializeLocation(final String serializedLocation) {
        final String[] data = serializedLocation.split(":");
        final Location location = new Location(Bukkit.getWorld(data[0]), Double.parseDouble(data[1]), Double.parseDouble(data[2]), Double.parseDouble(data[3]));
        location.setYaw(Float.parseFloat(data[4]));
        location.setPitch(Float.parseFloat(data[5]));
        return location;
    }

}
